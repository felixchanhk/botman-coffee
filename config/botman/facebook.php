<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Facebook Token
    |--------------------------------------------------------------------------
    |
    | Your Facebook application you received after creating
    | the messenger page / application on Facebook.
    |
    */
    'token' => env('FACEBOOK_TOKEN'),

    /*
    |--------------------------------------------------------------------------
    | Facebook App Secret
    |--------------------------------------------------------------------------
    |
    | Your Facebook application secret, which is used to verify
    | incoming requests from Facebook.
    |
    */
    'app_secret' => env('FACEBOOK_APP_SECRET'),

    /*
    |--------------------------------------------------------------------------
    | Facebook Verification
    |--------------------------------------------------------------------------
    |
    | Your Facebook verification token, used to validate the webhooks.
    |
    */
    'verification' => env('FACEBOOK_VERIFICATION'),

    /*
    |--------------------------------------------------------------------------
    | Facebook Start Button Payload
    |--------------------------------------------------------------------------
    |
    | The payload which is sent when the Get Started Button is clicked.
    |
    */
    'start_button_payload' => 'GET_STARTED',

    /*
    |--------------------------------------------------------------------------
    | Facebook Greeting Text
    |--------------------------------------------------------------------------
    |
    | Your Facebook Greeting Text which will be shown on your message start screen.
    |
    */
    'greeting_text' => [
        'greeting' => [
            [
                'locale' => 'default',
                'text' => 'Hello! {{user_first_name}}, nice to meet you',
            ],
            [
                'locale' => 'en_US',
                'text' => 'Timeless apparel for the masses.',
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Facebook Persistent Menu
    |--------------------------------------------------------------------------
    |
    | Example items for your persistent Facebook menu.
    | See https://developers.facebook.com/docs/messenger-platform/reference/messenger-profile-api/persistent-menu/#example
    |
    */
    'persistent_menu' => [
        [
            'locale' => 'default',
            'composer_input_disabled' => 'false',
            'call_to_actions' => [
                [
                    'title' => 'General Inquiry',
                    'type' => 'nested',
                    'call_to_actions' => [
                        [
                            'title' => 'Store Information',
                            'type' => 'postback',
                            'payload' => '/faq',
                        ],
                    	[
                            'title' => 'Check Order',
                            'type' => 'postback',
                            'payload' => '/order',
                        ],
                    	[
                            'title' => 'Latest News',
                            'type' => 'postback',
                            'payload' => '/news',
                        ],
                    	[
                            'title' => 'Get Coupon',
                            'type' => 'postback',
                            'payload' => '/survey',
                        ],
                    	[
                            'title' => 'Subscribe Newsletter',
                            'type' => 'postback',
                            'payload' => '/subscribe',
                        ],
                    ],
                ],
            	[
                    'title' => 'Others',
                    'type' => 'nested',
                    'call_to_actions' => [
                        [
                            'title' => 'Buy Product',
                            'type' => 'postback',
                            'payload' => 'buy coffee',
                        ],
                    	[
                            'title' => 'Leave Contact',
                            'type' => 'postback',
                            'payload' => '/leave_contact',
                        ],
                    ],
                ],
            	[
                    'title' => 'My Coupon',
                    'type' => 'nested',
                    'call_to_actions' => [
                        [
                            'title' => 'Show Coupons',
                            'type' => 'postback',
                            'payload' => 'show coupons',
                        ],
                    	[
                            'title' => 'Save Coupon',
                            'type' => 'postback',
                            'payload' => 'save coupon',
                        ],
                    	[
                            'title' => 'Used Coupon',
                            'type' => 'postback',
                            'payload' => 'used coupon',
                        ],
                    	[
                            'title' => 'Delete Coupon',
                            'type' => 'postback',
                            'payload' => 'delete coupon',
                        ],
                    ],
                ]
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Facebook Domain Whitelist
    |--------------------------------------------------------------------------
    |
    | In order to use domains you need to whitelist them
    |
    */
    'whitelisted_domains' => [
        'https://petersfancyapparel.com',
    ],
];
