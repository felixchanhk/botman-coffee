<?php
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Middleware\ApiAi;
use Automattic\WooCommerce\Client;
use BotMan\BotMan\Messages\Attachments\Video;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use BotMan\Drivers\Facebook\Extensions\ButtonTemplate;
use App\Http\Controllers\BotManController;
use App\Todo;
use App\Coupon;
use App\Customer;
use App\ErrorConversation;


$botman = resolve('botman');

$botman->hears('/start|GET_STARTED', function ($bot) {
	$user = $bot->getUser();

    	Customer::firstOrCreate([
        	'user_id' => $user->getId(),
        	'first_name' => $user->getFirstName(),
    		'last_name' => $user->getLastName(),
    	]);

    $bot->reply('Nice to meet you, I am the shop assistant');
	$bot->reply('You can call me Mocha. What can I help you?');
	$bot->reply('Send /faq inquire general store information');
	$bot->reply('Send /order inquire order details');
	$bot->reply('Send /quiz play the quiz game to win the big discount coupon');
	$bot->reply('Send /news get the latest offers and promotions');
	$bot->reply('Send /survey Participate in the survey to get discounts and sweepstakes');
	$bot->reply('Send /subscribe Subcribe our newsletter');
	
});



/*==========================   Dialogflow Usage  ====================================*/
$dialogflow = ApiAi::create(env('DIALOGFLOW_TOKEN'), 'zh-HK')->listenForAction();

// Apply global "received" middleware
$botman->middleware->received($dialogflow);

// Apply matching middleware per hears command
$botman->hears('input.welcome', function (BotMan $bot) {
    // The incoming message matched the "my_api_action" on Dialogflow
    // Retrieve Dialogflow information:
    $extras = $bot->getMessage()->getExtras();
    //$drink = $extras['apiParameters']['geo-city'];
    $apiReply = $extras['apiReply'];
    //$bot->reply("Ok, so " . $drink . ' it is! 👌🏻');
    $bot->reply($apiReply);
})->middleware($dialogflow);

$botman->hears('input.end', function (BotMan $bot) {
    $extras = $bot->getMessage()->getExtras();
    $apiReply = $extras['apiReply'];
    $bot->reply($apiReply);
})->middleware($dialogflow);

/*==========================   End Dialogflow  ====================================*/



/*==========================   Conversation  ====================================*/
$botman->hears('Start conversation', BotManController::class . '@startConversation');

$botman->hears('/faq|faq|shop information|open time', function ($bot) {
    $bot->startConversation(new App\Conversations\GeneralConversation);
});

$botman->hears('/survey|.*survey', function ($bot) {
    $bot->startConversation(new App\Conversations\SurveyConversation);
});

$botman->hears('/order|.*check.*order', function ($bot) {
    $bot->startConversation(new App\Conversations\OrderConversation);
});

$botman->hears('/getorder {id}', 'App\Http\Controllers\OrderController@byId');

$botman->hears('I want to buy coffee|any suggestion?|(.*)buy(.*)coffee(.*)|i want * cheap coffee|i want to buy it', function ($bot) {
    $bot->startConversation(new App\Conversations\BuyProductConversation);
});

/*==========================   Conversation  ====================================*/



/*==========================   Simply Question  ====================================*/
$botman->hears('/subscribe|subscribe|i want to subscribe*.|subscribe newsletter', function ($bot) {
	$user = $bot->getUser();

	$customer = Customer::where( 'user_id', $user->getId() )->get();
	$customer = Customer::find($customer[0]->id);
	Log::debug($customer);

    if ( !empty($customer) ) {
    	$customer->subscribe = 1;
        $customer->save();
    } else {
        Customer::create([
        	'user_id' => $user->getId(),
        	'first_name' => $user->getFirstName(),
    		'last_name' => $user->getLastName(),
    		'subscribe' => 1
    	]);
    }

    $bot->reply('Thanks for your subscribe, we will send the update news to you.');
});

$botman->hears('/unsubscribe|.*unsubscribe*.|i want to unsubscribe*.|unsubscribe newsletter', function ($bot) {
	$user = $bot->getUser();

	$customer = Customer::where( 'user_id', $user->getId() )->get();
	$customer = Customer::find($customer[0]->id);
	Log::debug($customer);

    if ( !empty($customer) ) {
    	$customer->subscribe = 0;
        $customer->save();
    } else {
        Customer::create([
        	'user_id' => $user->getId(),
        	'first_name' => $user->getFirstName(),
    		'last_name' => $user->getLastName(),
    		'subscribe' => 0
    	]);
    }

    $bot->reply('Already unsubcribe the newsletter.');
});

// save data to storage
$botman->hears('/news|hot product|new product', function ($bot) {
	$image = 'https://eshop.felixchan.xyz/wp-content/uploads/2019/05/coffee_product05.jpg';
    $message = OutgoingMessage::create('Below is our new product')->withAttachment(
		new Image($image)
	);
	$bot->reply($message);
	$bot->reply('https://eshop.felixchan.xyz/product/coffee/nescafe-gold-cappuccino-instant-coffee-sachets/');
});

// save data to storage
$botman->hears('(#[0-9]{8,12})', function ($bot, $tel) {
    $bot->userStorage()->save([
        'user_email' => $bot->getMessage()->getSender(),
        'tel' => $tel
    ]);
    $bot->reply('您嘅電話係: ' . $tel);
});

/*==========================   Simply Question  ====================================*/



/*==========================   Website Interaction  ====================================*/
$botman->hears('/our_vision', function ($bot) {
    $message = OutgoingMessage::create('CEO speech')->withAttachment(
        new Video('https://eshop.felixchan.xyz/wp-content/uploads/video/coffee_vision.mp4')
    );
    $bot->reply($message);
    $bot->reply('We love coffee, we know coffee.');
});

$botman->hears('/about_us', function ($bot) {
    $message = OutgoingMessage::create('What is our coffe')->withAttachment(
        new Video('https://eshop.felixchan.xyz/wp-content/uploads/video/introduction_coffee.mp4')
    );
    $bot->reply($message);
    $bot->reply('<a href="https://eshop.felixchan.xyz/product-category/coffee/" target="_blank">Click here to know our product</a>');
});

/*==========================   Website Interaction  ====================================*/



/*==========================   Coupon Wallet  ====================================*/

// Read Coupon Information
$botman->hears('show coupon*|show my coupon|.*coupon wallet*.', function ($bot) {
    $coupons = Coupon::where('invalid', false)
        ->where('user_id', $bot->getMessage()->getSender())
        ->get();
    if (count($coupons)) {
        $bot->reply('Your coupons are:');
    
        foreach ($coupons as $coupon) {
        	$image = 'https://esellercafe.com/wp-content/uploads/2017/09/shopify-shopcodes-qr-code.jpg';
        	$message = OutgoingMessage::create($coupon->id . ' - ' . $coupon->coupon_code)->withAttachment(
				new Image($image)
			);
			$bot->reply($message);
        }
    
    } else {
        $bot->reply('You do not have any coupon');
    }
});

// Create Coupon Code
$botman->hears('save*{coupon}', function ($bot, $coupon_code) {
    Coupon::create([
        'coupon_code' => $coupon_code,
        'desc' => '0',
        'expire_date' => '0',
        'user_id' => $bot->getMessage()->getSender()
    ]);
    $bot->reply('You added a new coupon "' . $coupon_code . '" to your coupon wallet');
});

$botman->hears('save coupon', function ($bot) {
    $bot->ask('Which coupon do you want to save?', function ($answer, $conversation) {
        Coupon::create([
            'coupon_code' => $answer,
            'desc' => '0',
            'expire_date' => '0',
            'user_id' => $conversation->getBot()->getMessage()->getSender()
        ]);
        $conversation->say('You save a new coupon for "' . $answer . '"');
    });
});

// Update Coupon
$botman->hears('used coupon {id}', function ($bot, $id) {
    //$coupon = Coupon::where('coupon_code',$id)->where('invalid', false)->get();//used to get the coupon object
	//Log::debug($coupon[0]->id);
	$coupon = Coupon::find($id);// get the coupon collection to fillfeel save() requirement

    if (is_null($coupon)) {
        $bot->reply('sorry, I do not find the todo "' . $id . '"');
    } else {
        $coupon->invalid = true;
        $coupon->save();
        $bot->reply('whohoo, you used "' . $coupon->coupon_code . '"!');
    }
});

$botman->hears('used coupon', function ($bot) {

	$bot->ask('Which coupon do you used?', function ($answer, $conversation) {
    	//$coupon = Coupon::where('coupon_code',$answer)->where('invalid', false)->get();
        $coupon = Coupon::find($answer);
    
        if (is_null($coupon)) {
        	$conversation->say('sorry, I do not find the coupon code "' . $answer . '"');
    	} else {
        	$coupon->invalid = true;
        	$coupon->save();
        	$conversation->say('whohoo, you used "' . $coupon->coupon_code . '"!');
    	}
    });

});

// Delete coupon
/*$botman->hears('delete*{id}', function ($bot, $id) {
	$coupon = Coupon::where('coupon_code',$id)->where('invalid', false)->get();
    $coupon = Coupon::find($coupon[0]->id);

    if (is_null($coupon)) {
        $bot->reply('sorry, I do not find the coupon "' . $coupon . '"');
    } else {
        $coupon->delete();
        $bot->reply('You successfully deleted todo "' . $coupon->coupon_code . '"');
    }
});*/

$botman->hears('delete coupon', function ($bot) {
	$bot->ask('Which coupon do you want to delete?', function ($answer, $conversation) {
    	//$coupon = Coupon::where('coupon_code',$answer)->where('invalid', false)->get();
        $coupon = Coupon::find($answer);
    
        if (is_null($coupon)) {
        	$conversation->say('sorry, I do not find the coupon code "' . $answer . '"');
    	} else {
        	$coupon->delete();
        	$conversation->say('You successfully deleted "' . $coupon->coupon_code . '"!');
    	}
    });
});

/*==========================   Coupon Wallet  ====================================*/



/*==========================   Start Quiz Game  ====================================*/
//$botman->hears('start quiz|.*play *game|do *quiz*.|', function (BotMan $bot) {
$botman->hears('/quiz|start quiz|play quiz game', function (BotMan $bot) {
    $bot->startConversation(new App\Conversations\QuizConversation);
});

$botman->hears('/highscore|highscore', function (BotMan $bot) {
    $bot->startConversation(new App\Conversations\HighscoreConversation());
})->stopsConversation();
/*==========================   Start Quiz Game  ====================================*/



/*==========================   Get User Information  ===============================*/
$botman->hears('information', function ($bot) {

    $user = $bot->getUser();

    $bot->reply('ID: ' . $user->getId());
    $bot->reply('Firstname: ' . $user->getFirstName());
    $bot->reply('Lastname: ' . $user->getLastName());
    $bot->reply('Username: ' . $user->getUsername());
    $bot->reply('Info: ' . print_r($user->getInfo(), true));
});
/*==========================   Get User Information  ===============================*/



/*==========================   Handle Error Conversation  ===============================*/
$botman->hears('help|i need to help|user guide', function ($bot) {
    $bot->reply('This is the helping information.');
	$bot->reply('Please type the keyword like check order or buy coffee to chat');
	$bot->reply('Otherwise, you can use like /faq hot key to find the information');
	$bot->reply('My database is limited so can\'t answer you every question, sorry.');
})->skipsConversation();

$botman->hears('stop|stop conversation|end*conversation', function ($bot) {
    $bot->reply('We stopped your conversation!');
})->stopsConversation();

$botman->hears('/leave_contact|Leave Contact', function ($bot) {
    $bot->reply('Please send us your tel or email, thanks');

})->stopsConversation();
/*==========================   Handle Error Conversation  ===============================*/

$botman->hears('.*cancel order', function ($bot) {
    $bot->reply('sorry, we can\'t cancel order on this way, please login to you account or contact our cs through Facebook messager to do the action, thanks.');
});

// Fallback
$botman->fallback(function ($bot) {
	$message = $bot->getMessage();
	
	ErrorConversation::create([
        'content' => $message->getText(),
        'user_id' => $bot->getMessage()->getSender()
    ]);
    $extras = $bot->getMessage()->getExtras();
    $apiReply = $extras['apiReply'];
    $bot->reply($apiReply);
    //$bot->reply("I will convey your valuable comments to the relevant colleagues to follow up.");
    //$bot->reply("Please leave your mobile number [e.g #61236123] for contact, thanks");
	
});
