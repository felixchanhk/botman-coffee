<?php

use Illuminate\Database\Seeder;
use App\Question;
use App\Answer;

class QuestionAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Question::truncate();
        Answer::truncate();
        $questionAndAnswers = $this->getData();

        $questionAndAnswers->each(function ($question) {
            $createdQuestion = Question::create([
                'text' => $question['question'],
                'points' => $question['points'],
            ]);

            collect($question['answers'])->each(function ($answer) use ($createdQuestion) {
                Answer::create([
                    'question_id' => $createdQuestion->id,
                    'text' => $answer['text'],
                    'correct_one' => $answer['correct_one'],
                ]);
            });
        });
    }

    private function getData()
    {
        return collect([
        	[
                'question' => 'In what country is coffee believed to have originated?',
                'points' => '10',
                'answers' => [
                    ['text' => 'Arabia', 'correct_one' => false],
                    ['text' => 'Ethiopia', 'correct_one' => true],
                    ['text' => 'England', 'correct_one' => false],
                ],
            ],
        	[
                'question' => 'The average seven-ounce cup of coffee contains about how much caffeine(milligrams)?',
                'points' => '15',
                'answers' => [
                    ['text' => '50 ~ 100', 'correct_one' => false],
                    ['text' => '115 ~ 175', 'correct_one' => true],
                    ['text' => '200 ~ 300', 'correct_one' => false],
                ],
            ],
        	[
                'question' => 'Coffee can be grown alongside which crop?',
                'points' => '10',
                'answers' => [
                    ['text' => 'Rice', 'correct_one' => false],
                    ['text' => 'Corn', 'correct_one' => false],
                    ['text' => 'Both', 'correct_one' => true],
                ],
            ],
        	[
                'question' => 'What country exports more coffee than any other?',
                'points' => '20',
                'answers' => [
                    ['text' => 'Jamaica', 'correct_one' => false],
                    ['text' => 'Turkey', 'correct_one' => false],
                    ['text' => 'Brazil', 'correct_one' => true],
                ],
            ],
        	[
                'question' => 'Only ___ beats coffee as a traded commodity.',
                'points' => '10',
                'answers' => [
                    ['text' => 'Water', 'correct_one' => false],
                    ['text' => 'Oil', 'correct_one' => true],
                    ['text' => 'Corn', 'correct_one' => false],
                ],
            ],
        ]);
    }
}
