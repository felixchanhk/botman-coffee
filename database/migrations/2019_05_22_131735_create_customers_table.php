<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
        	$table->string('user_id')->nullable();
        	$table->string('first_name')->nullable();
        	$table->string('last_name')->nullable();
        	$table->string('locale')->nullable();
        	$table->string('gender')->nullable();
        	$table->string('email')->nullable();
        	$table->string('tel_number')->nullable();
        	$table->string('birthday')->nullable();
        	$table->string('remarks')->nullable();
        	$table->boolean('subscribe')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
