<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;

class GeneralConversation extends Conversation
{

    public function askFAQ()
    {
        $question = Question::create('FAQ Section, please click below button to get the information')
            ->fallback('Unable to recongise the question')
            ->callbackId('create_faq')
            ->addButtons([
                Button::create('Registration and Login')->value('registration'),
                Button::create('Orders')->value('orders'),
                Button::create('Payments')->value('payments'),
                Button::create('Delivery')->value('delivery'),
                Button::create('About Us')->value('about'),
            ]);

        $this->ask($question, function (Answer $answer) {
            // Detect if button was clicked:
            if ($answer->isInteractiveMessageReply()) {
                // will be either 'registration' or 'orders'
                $selectedValue = $answer->getValue();
                switch ($selectedValue) {
                    case "registration":
                        $this->askRegistration();
                        break;
                    case "orders":
                        $this->askOrder();
                        break;
                    case "payments":
                        $this->askPayments();
                        break;
                    case "delivery":
                        $this->askDelivery();
                        break;
                    case "about":
                        $this->askAboutUs();
                        break;
                }
            }
        });
    }

    public function askRegistration()
    {
        $questionText = 'Please input below code to know more details:<br>
        #001: Do I need an account to place an order?<br>
        #002: How to create an account?<br>
        #003: How to log in?<br>
        #004: I forgot my password. What should I do?';

        $this->ask($questionText, 
        [
            [
                'pattern' => '#001',
                'callback' => function () {
                    $this->say('Of course not. You can place an order without creating an account. However, registration gives you various benefits:');
                    $this->say('Having an account you can:');
                    $this->say('• check your order history');
                    $this->say('• avoid entering shipping details every time you want to place an order');
                    $this->say('You can <a href="https://eshop.felixchan.xyz/my-account/" target="_blank">click here</a> to register your account');
                }
            ],
            [
                'pattern' => '#002',
                'callback' => function () {
                    $this->say('In order to create a Coffeedesk account, please follow these steps:');
                    $this->say('1. Click “LOG IN” on the upper right of the website.');
                    $this->say('2. Click “CREATE AN ACCOUNT”.');
                    $this->say('3. Enter your email address and accept the terms of our service and privacy policy. It will enable us to fulfil your future orders. Click “JOIN”. ');
                }
            ],
            [
                'pattern' => '#003',
                'callback' => function () {
                    $this->say('Follow these steps to log in:');
                    $this->say('1. Click “LOG IN” on the upper right of the website.');
                    $this->say('2. Enter your email address and password.');
                    $this->say('3. Click “LOG IN” to confirm.');
                    $this->say('4. If you’re having trouble logging in, click “Forgot your password?” and enter your registered email address. We will email you a link to reset password.');
                }
            ],
            [
                'pattern' => '#004',
                'callback' => function () {
                    $this->say('For your safety, we do not store any passwords. However, you can reset your password.');
                    $this->say('If you can\'t remember it, click “Forgot your password?” and enter your registered email address. We will email you a link to reset password.');
                }
            ]
        ]);
    }

    public function askOrder()
    {
        $questionText = 'Please send below code number to know more our details:<br>
        #001: How to place an order?<br>
        #002: How to check order status?<br>
        #003: Can i edit an order I have already placed?';

        $this->ask($questionText, 
        [
            [
                'pattern' => '#001',
                'callback' => function () {
                    $this->say('1. Choose a product and add it to cart.');
                    $this->say('2. A new window will open, with a confirmation of the product added to cart. To buy more, click “CONTINUE SHOPPING”. If you want to finalize the order, click “CHECKOUT”.');
                    $this->say('3. If you want to use a discount code, enter it in the proper field and click “ACTIVATE”. Next, choose payment and shipping method and click “NEXT”.');
                    $this->say('4. Enter your email and shipping address. In this step, you can also choose to wrap an item as a gift or request a special drawing (just ask in the comment section). Remember to fill in all the required fields. Next, click ”CONTINUE” to proceed to order summary.');
                    $this->say('5. Make sure that the details in “Order Summary” are correct. If everything is correct and you’re logged in, click “CONFIRM YOUR ORDER”. If you don’t have an account, accept the terms of our service and privacy policy first. It will enable us to fulfil your order. ');
                    $this->say('6. If you have chosen PayU or PayPal as a payment method, please make the payment in this step.');
                    $this->say('7. Hurray! Your order has been successfully placed and we’re on it! We have also sent you a confirmation email. You will be informed about further proceedings via email.');
                }
            ],
            [
                'pattern' => '#002',
                'callback' => function () {
                    $this->say('You can check current order status after logging in to your account, in “Purchase history” section.');
                    $this->say('You will also be informed about further status changes via email.');
                    $this->say('If you have any inquiries, please contact our customer service: hello@coffeebaba.com ');
                }
            ],
            [
                'pattern' => '#003',
                'callback' => function () {
                    $this->say('An order cannot be updated by the client.');
                    $this->say('We will be happy do it for you,');
                    $this->say('unless your order has already been packed.');
                }
            ]
        ]);
    }

    public function askPayments()
    {
        $questionText = 'Please send below code number to know more our details:<br>
        #001: What payment methods are available?<br>
        #002: When will my payment be confirmed?<br>
        #003: Can I pay on delivery?';

        $this->ask($questionText, 
        [
            [
                'pattern' => '#001',
                'callback' => function () {
                    $this->say('For your convenience, there are several payment methods to choose from:');
                    $this->say('Visa / Mastercard: Credit / Debit Card - Pay quickly and securely with your credit or debit card. The payment will be processed by PayU.');
                    $this->say('International wire transfer: As soon as you confirm your order, you will be redirected to a page with your individual details required for a bank transfer. The payment will be processed by PayU.');
                    $this->say('PayPal: Pay with your PayPal account or credit card and receive a payment confirmation at once.');
                }
            ],
            [
                'pattern' => '#002',
                'callback' => function () {
                    $this->say('It depends on the payment method you have chosen. Confirming a payment made via PayU takes up to several minutes.');
                    $this->say('In the case of bank transfer, it may take up to 3 working days.');
                }
            ],
            [
                'pattern' => '#003',
                'callback' => function () {
                    $this->say('We\'re sorry, payment on delivery is not available at the moment.');
                }
            ]
        ]);
    }

    public function askDelivery()
    {
        $questionText = 'Please send below code number to know more our details:<br>
        #001: When will I get my order?<br>
        #002: When will my order be dispatched?<br>
        #003: I have received the wrong product. What should I do?';

        $this->ask($questionText, 
        [
            [
                'pattern' => '#001',
                'callback' => function () {
                    $this->say('Most parcels are dispatched within 24 hours from placing order.');
                    $this->say('In fact, if an order is placed before 1:00 P.M., it is usually dispatched the same day.');
                    $this->say('It takes 2 to 5 business days to deliver a package, depending on the country of destination.');
                    $this->say('You can check the estimated time using the UPS delivery map below:');
                    $this->say('https://wwwapps.ups.com/maps_europe/results?loc=en_US');
                }
            ],
            [
                'pattern' => '#002',
                'callback' => function () {
                    $this->say('Most parcels are dispatched within 24 hours from placing order.');
                    $this->say('In fact, if an order is placed before 1:00 P.M., it is usually dispatched the same day.');
                    $this->say('In some cases order processing time is longer. We always provide that information in product description.');
                }
            ],
            [
                'pattern' => '#003',
                'callback' => function () {
                    $this->say('Remember to inform us about your order number (you can find it in confirmation emails and “Purchase history” section)');
                    $this->say('and about the product you have received. We promise to make up for our mistake in the blink of an eye!');
                }
            ]
        ]);
    }

    public function askAboutUs()
    {
        $this->say('We are the leading distributor of coffee- and tea-related products in Hong Kong. ');
        $this->say('As it turns out, we have already caffeinated Poland and it’s time for the whole of Asia now! ');
        $this->say('At the moment, our wide range of products includes more than 3000 items from over 150 brands from all over the world. ');
    }

    /**
     * Start the conversation
     */
    public function run()
    {
        $this->askFAQ();
    }
}
