<?php

namespace App\Conversations;

use App\Highscore;
use BotMan\BotMan\Messages\Conversations\Conversation;

class HighscoreConversation extends Conversation
{
    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->showHighscore();
    }
    private function showHighscore()
    {
        $topUsers = Highscore::topUsers(5);
    
        if (! $topUsers->count()) {
            return $this->say('The highscore is still empty. Be the first one! 👍');
        }
    
        $topUsers->transform(function ($user) {
        
        	$driver = $this->bot->getDriver()->getName();
        	$username = $user->name;   
        	
        	if($user->name == " "){
				$username = $user->chat_id;
            	//$username = substr($username, 0, strpos($username, '@')); @todo
            }
        
        	if ($driver == "Web") {
        		return "{$user->rank} - {$username} *{$user->points} points*<br>";
            }else{
            	return "{$user->rank} - {$username} *{$user->points} points*".chr(10);
            }
        });
    
        $this->say('Here is the current highscore. Do you think you can do better? Start the quiz: /startquiz.');
        $this->say('🏆 TOP 5 HIGHSCORE 🏆😄');
        $this->say($topUsers->implode("\n"), ['parse_mode' => 'Markdown']);
    }
}