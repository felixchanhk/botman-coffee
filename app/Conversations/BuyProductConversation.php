<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;

class BuyProductConversation extends Conversation
{
    protected $productDetails;

    /**
     * First question
     */
    public function askTaste()
    {
        $question = Question::create("what is your favorite taste of coffee?")
            ->fallback('Unable to ask question')
            ->callbackId('askTaste')
            ->addButtons([
                Button::create('Creamy')->value('creamy'),
                Button::create('Sweet')->value('sweet'),
                Button::create('Sour')->value('sour'),
                Button::create('Bitter')->value('bitter')
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'creamy') {
                    $this->promoteLatte();
                } else if ($answer->getValue() === 'sweet') {
                    $this->promoteCaramelMacchiato();
                } else if ($answer->getValue() === 'sour') {
                    $this->promoteCappuccino();
                } else if ($answer->getValue() === 'bitter') {
                    $this->promoteEspresso();
                }
            }else{
            	$this->say("please click the button or type stop to end the conversation");
            }
        });
    }

    public function promoteLatte()
    {
        // get info form the product api
        $this->productDetails = $this->getProductInfoAPI(94);

        $question = Question::create("Have you drink Latte before?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_latte')
            ->addButtons([
                Button::create('Yes')->value('yes'),
                Button::create('Nope')->value('no')
            ]);

        return $this->ask($question, function (Answer $answer) {

            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'yes') {
                    $this->say('that\'s great, our shop has some excellent Latte coffee, you can try some.');
                    $message = OutgoingMessage::create($this->productDetails->name)->withAttachment(
                        new Image($this->productDetails->images[0]->src)
                    );
                    $this->say($message);
                    $this->say('Price: USD' . $this->productDetails->sale_price);
                    $this->askBuyLatte();
                } else if ($answer->getValue() === 'no') {
                    $this->say('no problem, let me to introduce our shop excellent latte coffee to you.');
                    $message = OutgoingMessage::create($this->productDetails->name)->withAttachment(
                        new Image($this->productDetails->images[0]->src)
                    );
                    $this->say($message);
                    $this->say('Price: USD' . $this->productDetails->sale_price);
                    $this->askBuyLatte();
                }
            }else{
            	$this->say("please click the button or type stop to end the conversation");
            }
        });
    }

    public function askBuyLatte()
    {

        $question = Question::create("do you want to buy it now or read more about it?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_buy_latte')
            ->addButtons([
                Button::create('Buy now')->value('buy'),
                Button::create('Read more')->value('read')
            ]);

        return $this->ask($question, function (Answer $answer) {
            $driver = $this->bot->getDriver()->getName();

            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'buy') {
                    $this->askBuyLatteQTY();
                } else if ($answer->getValue() === 'read') {
                    if ($driver == "Web") {
                        $this->say('Please <a href="' . $this->productDetails->permalink . '" target="_blank">click here</a> to know more the details');
                    } else {
                        $this->say('Please click below link to know more the details');
                        $this->say($this->productDetails->permalink);
                    }
                    $this->say('enjoy~');
                }
            }else{
            	$this->say("please click the button or type stop to end the conversation");
            }
        });
    }

    public function askBuyLatteQTY()
    {
        $this->ask('How many do you want to buy?', function (Answer $answer) {

            $getText = $answer->getText();
            $this->QTY = $getText;
            $driver = $this->bot->getDriver()->getName();

            $this->say('Added ' . $this->QTY . ' to shopping cart, please click below link to check it.');
            if ($driver == "Web") {
                $this->say('<a href="https://eshop.felixchan.xyz/?add-to-cart=94&quantity=' . $this->QTY . '" target="_blank"> Click Here </a>');
            } else {
                $this->say('https://eshop.felixchan.xyz/?add-to-cart=94&quantity=' . $this->QTY);
            }
        });
    }


    public function promoteCaramelMacchiato()
    {
        $question = Question::create("Have you drink Caramel Macchiato before?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_latte')
            ->addButtons([
                Button::create('Yes')->value('yes'),
                Button::create('Nope')->value('no')
            ]);

        return $this->ask($question, function (Answer $answer) {

            // get info form the product api
            $this->productDetails = $this->getProductInfoAPI(89);

            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'yes') {
                    $this->say('That\'s great, our shop has excellent Caramel Macchiato coffee, you can try some.');
                    $message = OutgoingMessage::create($this->productDetails->name)->withAttachment(
                        new Image($this->productDetails->images[0]->src)
                    );
                    $this->say($message);
                    $this->say('Price: USD' . $this->productDetails->sale_price);
                    $this->askBuyCaramelMacchiato();
                } else if ($answer->getValue() === 'no') {
                    $this->say('Let me introduce our shop excellent Caramel Macchiato coffee to you.');
                    $message = OutgoingMessage::create($this->productDetails->name)->withAttachment(
                        new Image($this->productDetails->images[0]->src)
                    );
                    $this->say($message);
                    $this->say('Price: USD' . $this->productDetails->sale_price);
                    $this->askBuyCaramelMacchiato();
                }
            }else{
            	$this->say("please click the button or type stop to end the conversation");
            }
        });
    }

    public function askBuyCaramelMacchiato()
    {
        $question = Question::create("Do you want to buy it now or read more about it?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_buy_latte')
            ->addButtons([
                Button::create('Buy now')->value('buy'),
                Button::create('Read more')->value('read')
            ]);

        return $this->ask($question, function (Answer $answer) {
            $driver = $this->bot->getDriver()->getName();

            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'buy') {
                    $this->askBuyCaramelMacchiatoQTY();
                } else if ($answer->getValue() === 'read') {
                    if ($driver == "Web") {
                        $this->say('Please <a href="' . $this->productDetails->permalink . '" target="_blank">click here</a> to know more the details');
                    } else {
                        $this->say('Please click below link to know more the details');
                        $this->say($this->productDetails->permalink);
                    }
                    $this->say('enjoy~');
                }
            }else{
            	$this->say("please click the button or type stop to end the conversation");
            }
        });
    }

    public function askBuyCaramelMacchiatoQTY()
    {
        $this->ask('How many Caramel Macchiato do you want to buy?', function (Answer $answer) {

            $getText = $answer->getText();
            $this->QTY = $getText;
            $driver = $this->bot->getDriver()->getName();

            $this->say('Added ' . $this->QTY . ' to shopping cart, please click below link to check it.');
            if ($driver == "Web") {
                $this->say('<a href="https://eshop.felixchan.xyz/?add-to-cart=89&quantity=' . $this->QTY . '" target="_blank"> Click Here </a>');
            } else {
                $this->say('https://eshop.felixchan.xyz/?add-to-cart=89&quantity=' . $this->QTY);
            }
        });
    }

    public function promoteCappuccino()
    {
        $question = Question::create("Have you drink Cappuccino before?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_latte')
            ->addButtons([
                Button::create('Yes')->value('yes'),
                Button::create('Nope')->value('no')
            ]);

        return $this->ask($question, function (Answer $answer) {

            // get info form the product api
            $this->productDetails = $this->getProductInfoAPI(91);

            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'yes') {
                    $this->say('That\'s great, our shop has excellent Cappuccino coffee, you can try some.');
                    $message = OutgoingMessage::create($this->productDetails->name)->withAttachment(
                        new Image($this->productDetails->images[0]->src)
                    );
                    $this->say($message);
                    $this->say('Price: USD' . $this->productDetails->sale_price);
                    $this->askBuyCappuccino();
                } else if ($answer->getValue() === 'no') {
                    $this->say('Let me introduce our shop excellent Cappuccino coffee to you.');
                    $message = OutgoingMessage::create($this->productDetails->name)->withAttachment(
                        new Image($this->productDetails->images[0]->src)
                    );
                    $this->say($message);
                    $this->say('Price: USD' . $this->productDetails->sale_price);
                    $this->askBuyCappuccino();
                }
            }else{
            	$this->say("please click the button or type stop to end the conversation");
            }
        });
    }

    public function askBuyCappuccino()
    {
        $question = Question::create("Do you want to buy it now or read more about it?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_buy_latte')
            ->addButtons([
                Button::create('Buy now')->value('buy'),
                Button::create('Read more')->value('read')
            ]);

        return $this->ask($question, function (Answer $answer) {
            $driver = $this->bot->getDriver()->getName();

            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'buy') {
                    $this->askBuyCappuccinoQTY();
                } else if ($answer->getValue() === 'read') {
                    if ($driver == "Web") {
                        $this->say('Please <a href="' . $this->productDetails->permalink . '" target="_blank">click here</a> to know more the details');
                    } else {
                        $this->say('Please click below link to know more the details');
                        $this->say($this->productDetails->permalink);
                    }
                    $this->say('enjoy~');
                }
            }else{
            	$this->say("please click the button or type stop to end the conversation");
            }
        });
    }

    public function askBuyCappuccinoQTY()
    {
        $this->ask('How many Cappuccino do you want to buy?', function (Answer $answer) {

            $getText = $answer->getText();
            $this->QTY = $getText;
            $driver = $this->bot->getDriver()->getName();

            $this->say('Added ' . $this->QTY . ' to shopping cart, please click below link to check it.');
            if ($driver == "Web") {
                $this->say('<a href="https://eshop.felixchan.xyz/?add-to-cart=91&quantity=' . $this->QTY . '" target="_blank"> Click Here </a>');
            } else {
                $this->say('https://eshop.felixchan.xyz/?add-to-cart=91&quantity=' . $this->QTY);
            }
        });
    }

    public function promoteEspresso()
    {
        $question = Question::create("Have you drink Espresso before?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_espresso')
            ->addButtons([
                Button::create('Yes')->value('yes'),
                Button::create('Nope')->value('no')
            ]);

        return $this->ask($question, function (Answer $answer) {

            // get info form the product api
            $this->productDetails = $this->getProductInfoAPI(50);

            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'yes') {
                    $this->say('That\'s great, our shop has excellent Espresso coffee, you can try some.');
                    $message = OutgoingMessage::create($this->productDetails->name)->withAttachment(
                        new Image($this->productDetails->images[0]->src)
                    );
                    $this->say($message);
                    $this->say('Price: USD' . $this->productDetails->sale_price);
                    $this->askBuyEspresso();
                } else if ($answer->getValue() === 'no') {
                    $this->say('Let me introduce our shop excellent Espresso coffee to you.');
                    $message = OutgoingMessage::create($this->productDetails->name)->withAttachment(
                        new Image($this->productDetails->images[0]->src)
                    );
                    $this->say($message);
                    $this->say('Price: USD' . $this->productDetails->sale_price);
                    $this->askBuyEspresso();
                }
            }else{
            	$this->say("please click the button or type stop to end the conversation");
            }
        });
    }

    public function askBuyEspresso()
    {
        $question = Question::create("Do you want to buy it now or read more about it?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_buy_espresso')
            ->addButtons([
                Button::create('Buy now')->value('buy'),
                Button::create('Read more')->value('read')
            ]);

        return $this->ask($question, function (Answer $answer) {
            $driver = $this->bot->getDriver()->getName();

            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'buy') {
                    $this->askBuyEspressoQTY();
                } else if ($answer->getValue() === 'read') {
                    if ($driver == "Web") {
                        $this->say('Please <a href="' . $this->productDetails->permalink . '" target="_blank">click here</a> to know more the details');
                    } else {
                        $this->say('Please click below link to know more the details');
                        $this->say($this->productDetails->permalink);
                    }
                    $this->say('enjoy~');
                }
            }else{
            	$this->say("please click the button or type stop to end the conversation");
            }
        });
    }

    public function askBuyEspressoQTY()
    {
        $this->ask('How many Espresso do you want to buy?', function (Answer $answer) {

            $getText = $answer->getText();
            $this->QTY = $getText;
            $driver = $this->bot->getDriver()->getName();

            $this->say('Added ' . $this->QTY . ' to shopping cart, please click below link to check it.');
            if ($driver == "Web") {
                $this->say('<a href="https://eshop.felixchan.xyz/?add-to-cart=50&quantity=' . $this->QTY . '" target="_blank"> Click Here </a>');
            } else {
                $this->say('https://eshop.felixchan.xyz/?add-to-cart=50&quantity=' . $this->QTY);
            }
        });
    }

    public function getProductInfoAPI($id)
    {
        $productInfo = json_decode(
            file_get_contents('https://eshop.felixchan.xyz/wp-json/wc/v3/products/' .
                $id . '?consumer_key=' .
                env('CONSUMER_KEY') . '&consumer_secret=' .
                env('CONSUMER_SECRET'))
        );
        return $productInfo;
    }


    /**
     * Start the conversation
     */
    public function run()
    {
        $this->askTaste();
    }
}
