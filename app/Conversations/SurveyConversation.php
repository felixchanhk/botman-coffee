<?php

namespace App\Conversations;

use Exception;
use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use App\Survey;

class SurveyConversation extends Conversation
{
	protected $quesiton1 = 'Do you know our brand before?';
	protected $quesiton2 = 'How many cup of coffee willl you drink per day?';
	protected $quesiton3 = 'Did you like Latte or Espresso?';
	protected $quesiton4 = 'How much will you spend on coffee per month?';

	protected $ans1 = '';
	protected $ans2 = '';
	protected $ans3 = '';
	protected $ans4 = '';


	public function askQuestion_1(){
    	$question = Question::create($this->quesiton1)
            ->fallback('Unable to ask question')
            ->callbackId('ask_q_1')
            ->addButtons([
                Button::create('Yes')->value('yes'),
                Button::create('No')->value('no')
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'yes') {
                	$this->ans1 = $answer->getValue();
                    $this->askQuestion_2();
                } else if ($answer->getValue() === 'no') {
                	$this->ans1 = $answer->getValue();
                    $this->askQuestion_2();
                }
            }
        });
    }

	public function askQuestion_2(){
    
    	$this->ask($this->quesiton2, function (Answer $answer) {

        	$this->ans2 = $answer->getText();
        	$this->askQuestion_3();
        
        });
    
    }

	public function askQuestion_3(){
    
    	$question = Question::create($this->quesiton3)
            ->fallback('Unable to ask question')
            ->callbackId('ask_q_3')
            ->addButtons([
                Button::create('Latte')->value('latte'),
                Button::create('Espresso')->value('espresso')
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'latte') {
                	$this->ans3 = $answer->getValue();
                    $this->askQuestion_4();
                } else if ($answer->getValue() === 'espresso') {
                	$this->ans3 = $answer->getValue();
                    $this->askQuestion_4();
                }
            }
        });
    
    }

	public function askQuestion_4(){
    
    	$this->ask($this->quesiton4, function (Answer $answer) {

        	$this->ans4 = $answer->getText();
        
        	$this->say('Thanks for your opinion, below is $10 coupon, you can use it on our online shop');
        	$this->say('#coffee$10');
        	
        	Survey::create([
        		'q1_answer' => $this->ans1,
            	'q2_answer' => $this->ans2,
            	'q3_answer' => $this->ans3,
            	'q4_answer' => $this->ans4,
            	'user_id' => $this->bot->getMessage()->getSender()
    		]);
        
        });
    
    }

	/**
	 * Start the conversation.
	 *
	 * @return mixed
	 */
	public function run()
	{
		$this->askQuestion_1();
	}
}