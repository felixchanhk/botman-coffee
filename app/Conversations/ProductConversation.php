<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;

class ProductConversation extends Conversation
{
    /**
     * First question
     */
    public function askTaste()
    {
        $question = Question::create("what is your favorite taste of coffee?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Sweet')->value('sweet'),
                Button::create('Salty')->value('salty'),
                Button::create('Sour')->value('sour'),
                Button::create('Bitter')->value('bitter')
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'sweet') {
                    $this->say('sweet');
                } else if ($answer->getValue() === 'salty') {
                    $this->say('salty');
                } else if ($answer->getValue() === 'sour') {
                    $this->say('sour');
                } else if ($answer->getValue() === 'bitter') {
                    $this->say('bitter');
                }
            }
        });
    }

    public function askCoffeetType()
    {
        $this->ask('Do you drink latte before?', function (Answer $answer) {
            /* vaildate the input
        	$getText = $answer->getText();
        	if( trim($getText) === '' && !is_numeric($getText) ){
            	$this->say('Please input your order number');
            	return $this->repeat();
            }*/
            $getText = $answer->getText();
            $this->orderID = $getText;

            $this->orderDetails = json_decode(file_get_contents('https://eshop.felixchan.xyz/wp-json/wc/v3/orders/' . $this->orderID . '?consumer_key=' . env('CONSUMER_KEY') . '&consumer_secret=' . env('CONSUMER_SECRET')));

            $this->askBillingEmail();
        });
    }

    public function askBuyLatte()
    {
        $this->ask('Do you want to buy it now or read more about it?', function (Answer $answer) {
            $question = Question::create("what is your favorite taste of coffee?")
                ->fallback('Unable to ask question')
                ->callbackId('ask_reason')
                ->addButtons([
                    Button::create('Sweet')->value('sweet'),
                    Button::create('Salty')->value('salty'),
                    Button::create('Sour')->value('sour'),
                    Button::create('Bitter')->value('bitter')
                ]);

            return $this->ask($question, function (Answer $answer) {
                if ($answer->isInteractiveMessageReply()) {
                    if ($answer->getValue() === 'sweet') {
                        $this->say('sweet');
                    } else if ($answer->getValue() === 'salty') {
                        $this->say('salty');
                    } else if ($answer->getValue() === 'sour') {
                        $this->say('sour');
                    } else if ($answer->getValue() === 'bitter') {
                        $this->say('bitter');
                    }
                }
            });
        });
    }

    /**
     * Start the conversation
     */
    public function run()
    {
        $this->askTaste();
    }
}
