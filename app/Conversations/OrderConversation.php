<?php

namespace App\Conversations;

use Log;
use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;

class OrderConversation extends Conversation
{
	protected $orderID;

	protected $orderDetails;

	public function askOrderDetails()
	{
		$this->ask('What is your order number?', function (Answer $answer) {
        
			$getText = $answer->getText();
			$this->orderID = $getText;

			$this->orderDetails = @file_get_contents('https://eshop.felixchan.xyz/wp-json/wc/v3/orders/' . $this->orderID . '?consumer_key=' . env('CONSUMER_KEY') . '&consumer_secret=' . env('CONSUMER_SECRET'));
        
        	if($this->orderDetails != FALSE){
        		$this->orderDetails = json_decode($this->orderDetails);
            	$this->askBillingEmail();
            }else{
            	$this->say('Sorry, cannot find that order, please input again.');
            	$this->repeat();
            }
        	//Log::info($this->orderDetails);

		});
	}

	public function askBillingEmail()
	{
		$this->ask('What is your billing email?', function (Answer $answer) {
			$getText = $answer->getText();
			$getText = trim(strtoupper($getText)," ");
			$email = $this->orderDetails->billing->email;
			$email = trim(strtoupper($email)," ");

			if($getText == $email){

			$this->say('Below are your order details:');
			$this->say('Order ID: ' . $this->orderDetails->id . ' | Status: ' . $this->orderDetails->status);
			$this->say('Shipping Details: ' . $this->orderDetails->shipping->first_name . ' ' . $this->orderDetails->shipping->last_name . ', ' . $this->orderDetails->shipping->company . ', '
				. $this->orderDetails->shipping->address_1 . ', ' . $this->orderDetails->shipping->address_2 . ', ' . $this->orderDetails->shipping->city . ', ' . $this->orderDetails->shipping->state . ', '
				. $this->orderDetails->shipping->postcode . ', ' . $this->orderDetails->shipping->country);
            $num = count($this->orderDetails->line_items);
            for($i = 0; $i<$num; $i++){
            	$this->say('Product Details: ' . $this->orderDetails->line_items[$i]->name . ' | QTY: ' . $this->orderDetails->line_items[$i]->quantity . ' | Subtotal: USD' . $this->orderDetails->line_items[$i]->subtotal);
            }
			
			}else{
				$this->say('You input email is not correct, please input again');	
				$this->repeat();
			}
		
		});
	}

	/**
	 * Start the conversation.
	 *
	 * @return mixed
	 */
	public function run()
	{
		$this->askOrderDetails();
	}
}
