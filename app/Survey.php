<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $fillable = [
        'q1_answer', 
    	'q2_answer', 
    	'q3_answer', 
    	'q4_answer', 
    	'user_id'
    ];
}
