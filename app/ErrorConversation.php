<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ErrorConversation extends Model
{
    protected $fillable = [
        'content', 
    	'user_id'
    ];
}
