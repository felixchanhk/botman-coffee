<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
   protected $fillable = [
    	'coupon_code',
   		'desc',	
   		'expire_date',	
    	'user_id'
   ];
}
