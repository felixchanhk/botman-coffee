<?php

namespace App\Http\Controllers;

use App\Services\WoocommerceService;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Controller constructor
     * 
     * @return void
     */
    public function __construct()
    {
        $this->order = new WoocommerceService;
    }

    /**
     * Return a random dog image from all breeds.
     *
     * @return void
     */
    public function getOrderDetails($bot)
    {
        // $this->photos->random() is basically the photo URL returned from the service.
        // $bot->reply is what we will use to send a message back to the user.
        $bot->reply( $this->order->getOrder() );
    }

	/**
     * Return a random dog image from all breeds.
     *
     * @return void
     */
    public function byId($bot, $id)
    {
        // $this->photos->random() is basically the photo URL returned from the service.
        // $bot->reply is what we will use to send a message back to the user.
        $bot->reply($this->order->byId(65));
    }

}