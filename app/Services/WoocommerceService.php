<?php
namespace App\Services;

use Exception;
use GuzzleHttp\Client;

class WoocommerceService
{
    // The endpoint we will be getting a random image from.
    const RANDOM_ENDPOINT = 'https://dog.ceo/api/breeds/image/random';

    const GET_ORDER_ENDPOINT = 'https://eshop.felixchan.xyz/wp-json/wc/v3/orders/65?consumer_key=ck_5b97a32a902b657f09fcc1bace7c8bdb344428bb&consumer_secret=cs_d75f4bf0dcf7f72059cbb0bf99fb564f51bbc281';

    /**
     * Guzzle client.
     *
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     * DogService constructor
     * 
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client;
    }

    /**
     * Fetch and return a random image from all breeds.
     *
     * @return string
     */
    public function random()
    {
        try {
            // Decode the json response.
            $response = json_decode(
                // Make an API call an return the response body.
                $this->client->get(self::RANDOM_ENDPOINT)->getBody()
            );

            // Return the image URL.
            return $response->message;
        } catch (Exception $e) {
            // If anything goes wrong, we will be sending the user this error message.
            return 'An unexpected error occurred. Please try again later.';
        }
    }

    /**
     * Fetch and return a random image from a given breed.
     *
     * @param string $breed
     * @return string
     */
    public function byId($Id)
    {
        try {
            // We replace %s     in our endpoint with the given breed name.
            //$endpoint = sprintf(self::GET_ORDER_ENDPOINT, $Id);

            $response = json_decode(
                // Make an API call an return the response body.
                $this->client->get(self::GET_ORDER_ENDPOINT)->getBody()
            );

            return $response->message;
        } catch (Exception $e) {
            return "Sorry I couldn\"t get you any photos from $Id. Please try with a different breed.";
        }
    }
}
