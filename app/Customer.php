<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 
    	'first_name',
    	'last_name',
    	'profile_pic',
        'locale',
        'gender',
    	'subscribe',
    	'remarks'
    ];
}
